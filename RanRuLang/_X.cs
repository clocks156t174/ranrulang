﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace RanRuLang {
	public partial class _X {

		public static Dictionary<string,Func<_X,XmlNode,string>> _p_=new Dictionary<string,Func<_X,XmlNode,string>>();
		/// <summary>File Stack</summary>
		public List<string> fs=new List<string>();

		public Dictionary<string,string> v=null;

		public XmlDocument xmlDocument;
		/// <summary>output string</summary>
		public string y="";
		/// <summary></summary>
		/// <param name="x">file name</param>
		/// <param name="xv">Variables to send</param>
		public _X(string x,Dictionary<string,string>xv,IEnumerable<string>xfs) {
			if(xv==null) { v=new Dictionary<string,string>(); } else { v=new Dictionary<string,string>(xv); }
			if(xfs!=null) { fs.AddRange(xfs); }
			xmlDocument=new XmlDocument();
			xmlDocument.Load_(x);
			Reset();
			//
		}

		public string g_v(string x) {
			return v.ContainsKey(x)?v[x]:"";
		}

		public string p_(XmlNodeList x) {
			string yy="";
			for(int i=0;i<x.Count;i++) { yy=yy+p_(x[i]); }
			return yy;
		}

		public string p_(IEnumerable<XmlNode> x) {
			string yy="";
			foreach(var m in x) { yy=yy+p_(x); }
			return yy;
		}
		/// <summary>Process(or Parse) a node</summary>
		/// <param name="x"></param>
		public string p_(XmlNode x) {
			switch(x.NodeType) {
				case XmlNodeType.Attribute:
				break;
				case XmlNodeType.CDATA:
				break;
				case XmlNodeType.Comment:
				break;
				case XmlNodeType.Document:
				break;
				case XmlNodeType.DocumentFragment:
				break;
				case XmlNodeType.DocumentType:
				break;
				case XmlNodeType.Element: {
					string yk=!_p_.ContainsKey(x.Name)||_p_[x.Name]==null?"/":x.Name;
					string y="";
					int rpt=(int)VarOrNumber(x.Attr("repeat"));
					for(int i=-1;i<rpt;i++) {
						y=y+_p_[yk](this,x);
					}
					return y;
				}
				case XmlNodeType.EndElement:
				break;
				case XmlNodeType.EndEntity:
				break;
				case XmlNodeType.Entity:
				break;
				case XmlNodeType.EntityReference:
				break;
				case XmlNodeType.None:
				break;
				case XmlNodeType.Notation:
				break;
				case XmlNodeType.ProcessingInstruction:
				break;
				case XmlNodeType.SignificantWhitespace:
				break;
				case XmlNodeType.Text:
				return x.Value;
				case XmlNodeType.Whitespace:
				break;
				case XmlNodeType.XmlDeclaration:
				break;
				default:
				break;
			} return "";
		}

		public void Reset() { y=p_(xmlDocument.ChildNodes); }

		public double VarOrNumber(string x) {
			if(string.IsNullOrEmpty(x)) { return 0; }
			if(x[0]=='$') {
				x=x.Substring(1);
				return v.ContainsKey(x)?MathParser._.Parse(v[x]):0;
			}
			return MathParser._.Parse(x);
		}
		//VarOrString is not necessary. Use plain text or v tag.
	}
}