﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace RanRuLang {
	partial class _pr {

		public static void Load_(this XmlDocument x,string filename) {
			StreamReader zr=new StreamReader(filename.Contains("?")?filename.Substring(0,filename.IndexOf('?')):filename);
			long tick=DateTime.Now.Ticks;
			x.LoadXml("<r"+tick+">"+zr.ReadToEnd()+"</r"+tick+">");
			zr.Close();
		}

		public static string Attr(this XmlNode x, string key){
			XmlAttribute a=x.Attributes[key];
			return a==null?"":a.Value;
		}
	}
}