﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace RanRuLang {
	public class RandomX:Random{

		public string seed;

		public int counter=0;

		public RandomX():this(null){ }

		public RandomX(int x):this(x.ToString()){ }

		public RandomX(string x) { seed=x==null?DateTime.Now.Ticks.ToString():x; }

		public static byte[] md5(string x) {
			return ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(new UTF8Encoding().GetBytes(x));
		}

		public override int Next() {
			byte[] z=md5(seed+counter);
			counter++;
			return z[0]|(z[1]<<8)|(z[2]<<16)|((z[3]&127)<<24);
		}

		public override int Next(int maxValue) {
			if(maxValue==0||maxValue==1) { return 0; }
			if(maxValue<0) {
				if(maxValue==-2147483648) { return Next(); }
				return Next(-maxValue);
			}
			byte[] z=md5(seed+counter);
			counter++;
			long y=0;
			for(int i=0;i<z.Length;i++) {
				y=(y<<8)|z[i];
				y=y%maxValue;
			}
			return (int)(y&0x7fffffff);
		}

		public override int Next(int minValue,int maxValue) {
			return Next(maxValue-minValue);
		}

		public override void NextBytes(byte[] buffer) {
			if(buffer==null) { return; }
			int j;
			for(int i=0;i<buffer.Length;i+=16) {
				byte[] z=md5(seed+counter);
				counter++;
				for(j=0;j<16&&(i|j)<buffer.Length;j++) {
					buffer[i|j]=z[j];
				}
			}
		}

		public override double NextDouble() {
			byte[] z=md5(seed+counter);
			counter++;
			double y=0;
			for(int i=0;i<z.Length;i++) {
				y=(y+z[i])*0.00390625;
			}
			return y;
		}
	}
}