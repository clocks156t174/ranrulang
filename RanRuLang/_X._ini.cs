﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace RanRuLang {
	partial class _X {
		/// <summary>程式開始時候要執行一次，不然沒有作用</summary>
		public static void _ini() {
			///todo
			///calc
			///seedadd
			///seedcopy
			///seednew
			///seeduse
			_p_["/"]=delegate(_X x,XmlNode xn) {
				return x.p_(xn.ChildNodes);
				/*
				string y="";
				XmlNode m;
				foreach(var m__ in xn) {
					if((m=m__ as XmlNode)==null) { continue; }
					y=y+x.p_(m);
				}
				return y;
				//*/
			};
			_p_["c"]=delegate(_X x,XmlNode xn) {
				return xn.Attr("v");
			};
			_p_["f"]=delegate(_X x,XmlNode xn) {
				XmlAttribute zk_=xn.Attributes["k"];
				if(zk_==null) { return ""; }
				string zk=zk_.Value;
				if(x.fs.Contains(zk)) { return ""; }//looping
				if(!File.Exists(zk)) { return ""; }
				Dictionary<string,string> yv=new Dictionary<string,string>();
				var zcn=xn.ChildNodes;
				XmlNode zn;
				XmlAttribute zk_2;
				string zk2;
				for(int i=0;i<zcn.Count;i++) {//要傳送的記憶字串
					zn=zcn[i];
					if(zn.NodeType!=XmlNodeType.Element) { continue; }
					//if(zn.Name!="w") { continue; }//標籤名稱隨便打，目前只要確認有k屬性
					zk_2=zn.Attributes["k"];
					if(zk_2==null) { continue; }
					zk2=zk_2.Value;
					yv[zk2]=x.g_v(zk2);
				}
				return new _X(zk,yv,x.fs).y;
			};
			_p_["r"]=delegate(_X x,XmlNode xn) {
				//list all possible i
				XmlNodeList zl=xn.ChildNodes;
				List<int> zprob=new List<int>();
				List<XmlNode> zpsb=new List<XmlNode>();
				XmlNode zn;
				int zp=0;
				for(int i=0;i<zl.Count;i++) {
					zn=zl[i];
					if(zn.NodeType!=XmlNodeType.Element) { continue; }
					zp=int.Parse(zn.Attr("p"));//replace int.parse later
					if(zp<1) { continue; }
					zprob.Add(zp);
					zpsb.Add(zn);
				}
				//choose one
				if(zprob.Count<1) { return ""; }
				return x.p_(zpsb[_pr.SelectIndexWeighted(zprob)]);
			};
			_p_["v"]=delegate(_X x,XmlNode xn) {//read from Var
				return x.g_v(xn.Attr("k"));
			};
			_p_["w"]=delegate(_X x,XmlNode xn) {//Write to var
				x.v[xn.Attr("k")]=x.p_(xn.ChildNodes);
				return "";
			};
			_p_["w"]=delegate(_X x,XmlNode xn) {//Write to var
				try {
					return MathParser._.Parse(x.p_(xn.ChildNodes)).ToString();
				} catch(Exception) { return ""; }
			};
			_p_["eng_a"]=delegate(_X x,XmlNode xn) {//ENGlish A or an
				string y=x.v[xn.Attr("k")]=x.p_(xn.ChildNodes);

				return "a "+y;
			};
		}
	}
}