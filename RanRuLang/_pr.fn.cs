﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace RanRuLang {
	partial class _pr {

		public static int SelectIndexWeighted(IEnumerable<int>x) {
			int z=0;
			List<int> a=new List<int>();
			a.Add(0);
			foreach(int m in x) { a.Add(z=z+m); }
			z=random.Next(z);
			z=a.BinarySearch(z);
			if(z<0) { z=-2-z; }
			return z;
		}
	}
}